package xmath

import "github.com/shopspring/decimal"

// Sum 计算浮点数值之和
func Sum(f ...float64) float64 {
	sum := decimal.NewFromFloat(0)
	for _, v := range f {
		sum = decimal.NewFromFloat(v).Add(sum)
	}
	v, _ := sum.Float64()
	return v
}

// Sub 减法
func Sub(f ...float64) float64 {
	sum := decimal.NewFromFloat(0)
	for k, v := range f {
		if k == 0 {
			sum = decimal.NewFromFloat(v)
		} else {
			sum = sum.Sub(decimal.NewFromFloat(v))
		}
	}
	v, _ := sum.Float64()
	return v
}

// Mul 乘法
func Mul(f ...float64) float64 {
	if len(f) == 0 {
		return 0
	}
	sum := decimal.NewFromFloat(f[0])
	o := f[1:]
	for _, v := range o {
		sum = decimal.NewFromFloat(v).Mul(sum)
	}
	v, _ := sum.Float64()
	return v
}

// Div 除法 f1/f2
func Div(f1, f2 float64) float64 {
	if f2 == 0 {
		return 0
	}
	f2Decimal := decimal.NewFromFloat(f2)
	r := decimal.NewFromFloat(f1).Div(f2Decimal)
	v, _ := r.Float64()
	return v
}
