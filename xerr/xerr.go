package xerr

import (
	"github.com/pkg/errors"
	"time"
)

type XCode int

type Error struct {
	code XCode
	msg  string
	err  error
	at   time.Time
}

func New(code XCode) *Error {
	e := &Error{
		code: code,
		at:   time.Now(),
	}
	return e
}

func (e *Error) SMsg(m string) *Error {
	e.msg = m
	return e
}

func (e *Error) SErr(err error) *Error {
	e.err = err
	return e
}

func (e *Error) Code() XCode {
	return e.code
}

func (e *Error) Error() string {
	return e.msg
}

func (e *Error) At() time.Time {
	return e.at
}

func (e *Error) Err() error {
	return e.err
}

func (e *Error) ToXError(s error) *Error {
	err, _ := s.(*Error)
	return err
}

func Cause(err error) error {
	return errors.Cause(err)
}

func Wrap(err error, msg string) error {
	return errors.Wrap(err, msg)
}

func WithMessage(err error, msg string) error {
	return errors.WithMessage(err, msg)
}

func WithStack(err error) error {
	return errors.WithStack(err)
}
