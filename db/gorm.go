package db

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
)

type Options struct {
	Addr     string
	User     string
	Password string
	Database string
	Debug    bool
}

func New(opts *Options) *gorm.DB {
	var (
		logLevel logger.LogLevel
	)

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", opts.User, opts.Password, opts.Addr, opts.Database)

	if opts.Debug {
		logLevel = logger.Info
	} else {
		logLevel = logger.Silent
	}

	return conn(dsn, logLevel)
}

//func NewFromConsul(opts *Options, consul consul.Consul) *gorm.DB {
//	var (
//		logLevel    logger.LogLevel
//	)
//
//	addr, err := consul.KVGet(opts.Addr)
//	if err != nil {
//		panic(err)
//	}
//
//	user, err := consul.KVGet(opts.User)
//	if err != nil {
//		panic(err)
//	}
//
//	password, err := consul.KVGet(opts.Password)
//	if err != nil {
//		panic(err)
//	}
//
//	database, err := consul.KVGet(opts.Database)
//	if err != nil {
//		panic(err)
//	}
//
//	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", user, password, addr, database)
//
//	if opts.Debug {
//		logLevel = logger.Info
//	} else {
//		logLevel = logger.Silent
//	}
//
//	return conn(dsn, logLevel)
//}

func conn(dsn string, level logger.LogLevel) *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			LogLevel: level,
			Colorful: true,
		},
	)

	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                      dsn,
		DisableDatetimePrecision: true,
	}), &gorm.Config{
		Logger: newLogger,
		TranslateError: true,
	})

	if err != nil {
		panic(err)
	}

	return db
}
