package main

import (
	"fmt"
	"gitee.com/jiewen/go-toolkit/conf"
	"log"
)

func main() {
	yaml()
	fmt.Println()
	ini()
}

func yaml() {
	c, err := conf.SGetConfigs("yaml", "./configs")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(c.Get("Hacker"))
	fmt.Println(c.GetString("name"))
	fmt.Println(c.GetInt("age"))
	fmt.Println(c.GetStringSlice("hobbies")[0])
	fmt.Println(c.GetStringMap("testing"))
	fmt.Println(c.GetStringMap("clothing")["jacket"])

}

func ini() {
	c, err := conf.SGetConfigs("ini", "./configs/testini.ini")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(c.GetString("Server.Addr"))
	fmt.Println(c.GetBool("Server.testbool"))
	fmt.Println(c.GetInt("Server.testint"))
	fmt.Println(c.GetFloat64("Server.testfloat"))
}
