package main

import (
	"fmt"
	"log"
	"wanbi-pkg/zip"
)

func main() {

	//fmt.Println(filepath.VolumeName(`C:\foo\bar`))
	//fmt.Println(filepath.Rel("", `D:\testdata`))
	//return
	//fmt.Println(filepath.Clean(`./a\b//c/../d\`))
	//fmt.Println(filepath.Rel("c:/windows", "c:/windows/system/"))
	//fmt.Println(filepath.Rel("", "c:/windows/system/"))
	//fmt.Println(filepath.Rel("/a/b/c/d", "/a/b/c/d/"))
	//return
	//compress()
	//compressSpecifyPositionFiles()
	//readFile()
	//compress2()
	//unCompress2()
	readFile2()
	//compressSpecifyPositionFiles2()
	//compress2Win()
}

//func compressSpecifyPositionFiles() {
//	_zip := zip.NewV1()
//	//files := []string{
//	//	"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata",
//	//	//"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata/file/dir2",
//	//	//"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata/file/1111.log",
//	//	//"./testdata/file/dir1",
//	//	//"./testdata/file/dir2",
//	//	//"./testdata/file/1111.log",
//	//	//"./testdata/file/abc.txt",
//	//}
//	savePath := "./testdata/zip/test.zip"
//	var files = map[string]string{
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata/file": "./",
//		"./testdata/file/dir1": "./",
//	}
//	if err := _zip.CompressSpecifyPositionFiles(files, savePath, true); err != nil {
//		log.Fatal(err)
//	}
//}
//
//func compress() {
//	_zip := zip.New()
//	files := []string{
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/2.log",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/a.log",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/bgt.log",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir2/",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir2/abc.txt.log",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir3/",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir4/",
//		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir5/",
//
//		//"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata",
//		//"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata/file/dir2",
//		//"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/pkg/examples/zip/testdata/file/1111.log",
//		//"./testdata/file/dir1",
//		//"./testdata/file/dir2",
//		//"./testdata/file/1111.log",
//		//"./testdata/file/abc.txt",
//	}
//	//fmt.Println(filepath.Dir("/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata"))
//	savePath := "./testdata/zip/test.zip"
//	prefix := "/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/"
//	if err := _zip.Compress(files, savePath, false, filepath.Dir(prefix)); err != nil {
//		log.Fatal(err)
//	}
//}
//
//func unCompress() {
//	_zip := zip.New()
//	zipPath := "./testdata/zip/5065.zip"
//	savePath := "./testdata/zip"
//	if err := _zip.UnCompress(zipPath, savePath, false); err != nil {
//		log.Fatal(err)
//	}
//}
//
//func readFile() {
//	//var path = "/Users/qingwenjie/Docker/php-nginx/conf.d/grpc.conf"
//	path := `C:\Windows\addins\FXSEXT.ecf`
//	contentByte, err := zip.New().ReadFile(
//		"./testdata/zip/28822.zip",
//		//`./C:Windows/addins/FXSEXT.ecf`,
//		path,
//	)
//	if err != nil {
//		log.Fatal(err)
//	}
//	fmt.Println(string(contentByte))
//}

func compress2() {
	files := []string{
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/1111.txt",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/2.log",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/a.log",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/bgt.log",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir2/",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir2/abc.txt.log",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir3/",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir4/",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir5/",
	}

	opt := &zip.CompressOptions{
		Files:       files,
		SavePath:    "./testdata/zip/testv2.zip",
		FilterPath:  "/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata",
		IsRecursive: false,
	}

	z := zip.New()
	if err := z.Compress(opt); err != nil {
		log.Fatal(err)
	}
}

func unCompress2() {
	z := zip.New()

	opt := &zip.UnCompressOptions{
		SrcPath:   "./testdata/zip/testv2.zip",
		DstPath:   "./testdata/zip/1",
		Overwrite: false,
	}
	if err := z.UnCompress(opt); err != nil {
		log.Fatal(err)
	}
}

func readFile2() {
	z := zip.New()
	res, err := z.Read(
		//"./testdata/zip/28822.zip",
		//`C:\Windows\addins\FXSEXT.ecf`,
		`./testdata/zip/839461.zip`,
		`opt/ltc001/ltc001.txt`,
	)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(res))
}

func compressSpecifyPositionFiles2() {
	z := zip.New()
	files := map[string]string{
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/1111.txt":    "a/p1111.txt",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/":      "b/pdir",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/2.log": "c/p2.log",
		"/Users/qingwenjie/Work/Golang/GuanChen/wanbi/client/testdata/dir11/a.log": "d/pa.log",
	}

	opt := &zip.CompressSpecifyPositionFilesOptions{
		Files:       files,
		SavePath:    "./testdata/zip/testv2_2.zip",
		IsRecursive: false,
	}

	if err := z.CompressSpecifyPositionFiles(opt); err != nil {
		log.Fatal(err)
	}
}

func compress2Win() {
	files := []string{
		`C:\testdata`,
		`D:\testdata`,
	}

	opt := &zip.CompressOptions{
		Files:       files,
		SavePath:    "./testdata/zip/testv2_win.zip",
		//FilterPath:  `D:\testdata`,
		IsRecursive: true,
	}

	z := zip.New()
	if err := z.Compress(opt); err != nil {
		log.Fatal(err)
	}
}
