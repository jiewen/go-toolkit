package main

import (
	"fmt"
	"log"

	goredis "gitee.com/jiewen/go-toolkit/redis"
)

func main() {
	redisConn := goredis.NewPool(&goredis.Options{
		Protocol: "tcp",
		Addr:     "127.0.0.1:6379",
		Password: "123456",
		Database: 0,
	})

	reply, err := redisConn.Get().Do("KEYS", "*")

	// reply, err := redisConn.Do("KEYS", "*")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(reply)
}

// func pool(addr, password string) *redis.Pool {
// 	dialOption := redis.DialPassword(password)

// 	return &redis.Pool{
// 		MaxIdle:     3,
// 		IdleTimeout: 240 * time.Second,
// 		Dial: func() (redis.Conn, error) {
// 			return redis.Dial("tcp", addr, dialOption)
// 		},
// 		TestOnBorrow: func(c redis.Conn, t time.Time) error {
// 			_, err := c.Do("PING")
// 			return err
// 		},
// 	}
// }
