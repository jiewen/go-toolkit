package main

import (
	"errors"
	"gitee.com/jiewen/go-toolkit/examples/xerr/xerrcode"
	"gitee.com/jiewen/go-toolkit/logger"
	"gitee.com/jiewen/go-toolkit/xerr"
)

func main() {
	log := newLogger()
	mockErr1 := newErr()
	//err1 := xerr.WithStack(mockErr1)

	mockErr2 := errors.New("1111")
	err2 := xerr.WithStack(mockErr2)
	err22 := xerr.WithStack(err2)

	e, ok := mockErr1.(*xerr.Error)
	if !ok {
		log.Fatal("not ok")
	}

	log.Debug(e.Code())
	log.Debug(e.Err())
	log.Debug(e.Error())
	log.Debug(e.At())

	log.ErrorV(err22)
}

func newErr() error {
	e := errors.New("err msg1")
	return xerr.New(xerrcode.DemoErr).SMsg("err msg2").SErr(e)
}

func newLogger() logger.Logger {
	return logger.New(
		logger.SetLevel(logger.DebugLevel),
		logger.SetOut("stdout"),
		logger.SetFileNameRule(true),
	)
}
