package xerrcode

import "gitee.com/jiewen/go-toolkit/xerr"

const (
	DemoErr xerr.XCode = 100
)

var Code2Value = map[xerr.XCode]string{
	DemoErr: "自定义错误",
}
