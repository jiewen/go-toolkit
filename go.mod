module gitee.com/jiewen/go-toolkit

go 1.16

require (
	github.com/gomodule/redigo v1.8.4 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible // indirect
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	gorm.io/driver/mysql v1.0.5 // indirect
)
