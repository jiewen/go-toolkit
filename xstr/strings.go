package xstr

import (
	"bytes"
	"crypto/rand"
	"math/big"
)

func RandomString(l int) string {
	s := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	var container string
	b := bytes.NewBufferString(s)
	length := b.Len()
	bigInt := big.NewInt(int64(length))
	for i := 0; i < l; i++ {
		randomInt, _ := rand.Int(rand.Reader, bigInt)
		container += string(s[randomInt.Int64()])
	}
	return container
}

// Cut l=0时 不限长度
func Cut(s string, start int, l int) string {
	r := []rune(s)
	rl := len(r)

	from := start
	to := start + l

	if to > rl {
		to = rl
	}
	if l > 0 {
		return string(r[from:to])
	}
	return string(r[from:])
}

func Len(s string) int {
	r := []rune(s)
	return len(r)
}
